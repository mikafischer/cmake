genex-LINK_LIBRARY-FRAMEWORK-supports-suffix
--------------------------------------------

The :genex:`$<LINK_LIBRARY>` generator expression gains the capability, for the
``FRAMEWORK`` features, to handle the suffix of the framework library name.
